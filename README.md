Distribuované programové systémy
================================

Zadanie: key-value databáza inšpirovaná Amazon Dynamo

## Autori

* Roman Kopšo
* Patrik Krajča
* Patrik Pernecký
* Katarína Szakszová

## Technológie

* service discovery - Consul, Consul-Template & nginx
-> [http://blog.neilni.com/2015/09/14/consul-and-consul-template-with-docker-compose/](Link URL)
* load-balancer, proxy - nginx
* Docker
* logy, monitoring - ElasticSearch + Logstash + Kibana & Filebeat, Cadvisor
-> [http://evanhazlett.com/2014/11/Logging-with-ELK-and-Docker/](Link URL), [http://eembsen.github.io/2015/12/05/docker-es-filebeat/](Link URL)

## Spustenie

1. Cez docker-machine treba vytvoriť vm hostov „master“ a „slave“.
1. Po vytvorení sa treba presunúť do priečiku /docker/master zadať príkaz „eval $(docker-machine env master)“ a spustiť skript ./start.sh.
1. Po korektnom spustení master vm hosta a skriptu ./start.sh, to isté zopakovať aj pre slave. Presunúť sa do priečinku /docker/slave zadať príkaz „eval $(docker-machine env slave)“ a spustiť skript ./start.sh.
1. Pre prezeranie logov v Kibane, treba ísť na adresu master_ip:5601 zvoliť si nami vytvorený Dashboard pomocou Load -> advanced -> browse ./docker/Dashboard.
1. Service discovery consul je na adrese master_ip:8500, monitorovanie master/slave_ip:4500.
1. Na interakciu s dynamo db, treba ísť na master_ip:9090 kde je web GUI.
1. Na vypnutie a zrušenie našej infrakštruktúry stačí zavolať príkaz ./end.sh (na VM slave aj ./clear.sh).

##View
* Consul -> [http://master_ip:8500](Link URL)

* Kibana -> [http://master_ip:5601](Link URL)   - Load - Advanced - ./docker/Dashboard

* Cadvisor -> [http://master_ip:4500](Link URL)      [http://slave_ip:4500](Link URL)

* Web APP -  -> [http://master_ip:9090](Link URL) 

* fault-tolerance log -  ./docker/master/faultTolerance.log
* health check log -  ./docker/slave/check_health.log
* monitoring -    ./docker/master/monitor.sh    ./docker/slave/monitor.sh