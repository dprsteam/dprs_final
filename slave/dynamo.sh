#!/bin/bash 

DOCKER_IP=$(ifconfig docker0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)
VM_MASTER_IP=$(docker-machine ip master)
VM_SLAVE_IP=$(docker-machine ip slave1)
DYNAMO=$(docker images | grep "dynamo" | wc -l)
NO_OF_DYNAMOS=$(docker ps -a | grep "slave_dynamo" | wc -l)

if [ $DYNAMO -eq "0" ] ; then
	docker build -t dynamo dynamo/
fi

while [ $NO_OF_DYNAMOS -lt $1 ]
do
	sleep 2    
	PORT=$RANDOM
	docker run -d -p ${PORT}:4567 \
	-e SERVICE_NAME=dynamo \
	-e SERVICE_TAGS=production \
	-e DOCKER_IP=${DOCKER_IP} \
	-e CONSUL_IP=${VM_MASTER_IP} \
	-e IP=${VM_SLAVE_IP} \
	-e PORT=${PORT} \
	--name slave_dynamo_${PORT} dynamo 
    NO_OF_DYNAMOS=$(docker ps -a | grep "slave_dynamo" | wc -l)
done
