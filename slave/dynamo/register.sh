#!/bin/bash

#register yourself to consul
read -r -d '' MSG << EOM
	{	
		"Node": "slave_node",
   		"Address": "$DOCKER_IP",
  	    "Service": {
			"ID": "$SERVICE_NAME-$PORT",			
			"Service": "$SERVICE_NAME", 
			"Tags": [
				"$SERVICE_TAGS",
				"$1"
			],			
			"Address": "$IP",	
			"Port": $PORT
		},
		"Check": {
			"Node": "slave_node",
			"CheckID": "service:$SERVICE_NAME-$PORT",
			"Name": "Dynamo health check",
			"Status": "passing",
			"ServiceID": "$SERVICE_NAME-$PORT"
		}
	}
EOM
curl -v -X PUT -d "$MSG" http://$CONSUL_IP:8500/v1/catalog/register


#get all dynamo addresses from consul and store them to array
i=0;
for j in `curl -XGET --connect-timeout 5 $CONSUL_IP:8500/v1/catalog/service/dynamo 2>&1 | grep -oP '(?<=ServiceAddress).*?(?=}|$)' | awk -F "\"" '{print $3$6}'` 
do
   array[$i]=$j; 
    i=$(($i+1)); 
done

#do broadcast for other dynamos to download updated dynamo address list  
for address in "${array[@]}"
do
  curl -XGET http://$address/update
done
