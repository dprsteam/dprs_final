#!/bin/bash

#get all dynamo addresses from consul and store them to array
i=0;
for j in `curl -XGET --connect-timeout 5 $CONSUL_IP:8500/v1/catalog/service/dynamo 2>&1 | grep -oP '(?<=ServiceAddress).*?(?=}|$)' | awk -F "\"" '{print $3$6}'` 
do
   array[$i]=$j; 
    i=$(($i+1)); 
done

#register yourself to consul
read -r -d '' MSG << EOM
	{	
		"Node": "slave_node",
   		"Address": "172.17.0.1",
  	    "Service": {
			"ID": "dynamo-1000",			
			"Service": "dynamo", 
			"Tags": [
				"production",
				"$1"
			],			
			"Address": "192.168.99.101",	
			"Port": 1000
		},
		"Check": {
			"Node": "slave_node",
			"CheckID": "service:dynamo-1000",
			"Name": "Dynamo health check",
			"Status": "passing",
			"ServiceID": "dynamo-1000"
		}
	}
EOM
curl -v -X PUT -d "$MSG" http://192.168.99.100:8500/v1/catalog/register


#do broadcast for other dynamos to download updated dynamo address list  
for address in "${array[@]}"
do
  curl -XGET http://$address/dynamo/a
done
