#!/bin/bash

#deregister yourself from consul
read -r -d '' MSG << EOM
	{
	    "Node": "slave_node",
  	    "ServiceID": "$SERVICE_NAME-$PORT"
	}
EOM
curl -v -X PUT -d "$MSG" http://$CONSUL_IP:8500/v1/catalog/deregister

