#!/bin/bash

#get all dynamo addresses from consul
curl -XGET --connect-timeout 5 $CONSUL_IP:8500/v1/catalog/service/dynamo 2>&1 | grep -oP '(?<=production).*?(?=}|$)' | awk -F "\"" '{print $7 $10 "-" $3 }'

