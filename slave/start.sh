#!/bin/bash 
source env.sh 

rm -rf docker-compose.yml 
envsubst < "template.yml" > "docker-compose.yml"
envsubst < "./filebeat/template.yml" > "./filebeat/filebeat.yml"
docker-compose up -d
#./dynamo.sh 1
./testdynamo.sh

