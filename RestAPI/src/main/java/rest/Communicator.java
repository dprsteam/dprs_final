package rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Communicator {
	
	public String forwardReadRequest(String dynamoAddress, String id, String read) throws IOException, InterruptedException{
		  
    	String[] command = {  "/bin/bash", "-c", "curl -s -X GET http://" + dynamoAddress + "/dynamo?id=" + id + "\\&read=" + read}; 
    	Process process = Runtime.getRuntime().exec(command);     
 	    process.waitFor();
 	    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
 	    String response;
 	    response = reader.readLine();                            
 	        
 	    return response;
	}
	
	public String forwardWriteRequest(String dynamoAddress, String value, String id, String write) throws IOException, InterruptedException{
  
    	String[] command = {  "/bin/bash", "-c", "curl -s -X POST http://" + dynamoAddress + "/dynamo?value=" + value + "\\&id=" + id + "\\&write=" + write}; 
    	Process process = Runtime.getRuntime().exec(command);     
 	    process.waitFor();
 	    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
 	    String response;
 	    response = reader.readLine();                            
 	        
 	    return response;
	}
	
	public String replicate(String dynamoAddress, String value, String id) throws IOException, InterruptedException{ 
    	
    	String[] command = {"/bin/bash", "-c", "curl -s -X POST http://" + dynamoAddress + "/replicate?value=" + value + "\\&id=" + id}; 
    	Process process = Runtime.getRuntime().exec(command);     
 	    process.waitFor();
 	    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
 	    String response;
 	    response = reader.readLine();                            
 	        
 	    return response;
	}
	
	public void synchronize(String dynamoAddress, String begin, String end, String hash, String senderAddress) throws IOException, InterruptedException{
		Runtime run = Runtime.getRuntime();
    	run.exec(new String[] { "/bin/bash", "-c", "curl -s -X POST http://" + dynamoAddress + "/synchronize?begin=" + begin + "\\&end=" + end + "\\&hash=" + hash + "\\&sender=" + senderAddress}).waitFor();  
	}
	
	public String ping(String dynamoAddress) throws IOException, InterruptedException{
		
		String[] command = { "/bin/bash", "-c", "curl -s -X GET http://" + dynamoAddress + "/ping"}; 
		Process process = Runtime.getRuntime().exec(command);     
 	    process.waitFor();
 	    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
 	    String response;
 	    response = reader.readLine();                            
 	        
 	    return response;
	}
	
	public String take(String dynamoAddress, String id) throws IOException, InterruptedException{
			
		String[] command = { "/bin/bash", "-c", "curl --fail -s -X GET http://" + dynamoAddress + "/take/" + id}; 
		Process process = Runtime.getRuntime().exec(command);     
 	    process.waitFor();
 	    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
 	    String response;
 	    response = reader.readLine();                            
 	        
 	    return response;
	}
}
