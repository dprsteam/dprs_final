package rest;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

public class DataHandler {
	
    private Map<String, Values> values = new HashMap<String, Values>();
	
	public Values getValue(String id){
		return values.get(id);
	}
	
	public Values putValue(String some, String id){
		Values value = new Values(some, id);
		values.put(value.getID(), value);
		return value;
	}
	
	public String putValueAndWriteVersion(String some, String id){
			
		//If this is the first time, we are writing this data, we only generate vector clock
		if(!values.containsKey(id)){
			String vector = generateVectorClock(id);
			some = some + "-" + vector;
			Values value = new Values(some, id);
			values.put(value.getID(), value);
			return some;
		}
		
		//This one is updating data, we increment vector clock
		String valueWithVector = values.get(id).getValue();
		String vector = incrementVectorClock(id, valueWithVector);
		valueWithVector = some + "-" + vector;
		Values value = values.get(id);
		value.setValue(valueWithVector);
		
		return valueWithVector;
	}
	
	public Values updateValue(String some, String id){
		Values value = values.get(id);
		if(value == null){
			throw new IllegalArgumentException("No value with id " + id + " found");
		}
		value.setValue(some);
		return value;
	}
	
	public boolean hasId(String id){
		return values.containsKey(id);
	}
	
	public boolean isEmpty(){
		return values.isEmpty();
	}
	
	public void clearData(){
		values.clear();
	}
	
	private int vectorPositionToBeIncremented(String id){
		
		List<String> sorted_dynamo_addr = RestApi.dynamo_addr;
		int positionOfVectorToBeIncremented = 0;
		int startFromPosition = 0;
		int valueId = Integer.parseInt(id);
		
		for(int i = 0; i < sorted_dynamo_addr.size(); i++){
			int dynamoId = Integer.parseInt(sorted_dynamo_addr.get(i).split("-")[1]);
			if(dynamoId >= valueId){
				startFromPosition = i;
				break;
			}
		}		
		
		//We will find out which position of the vector clock will be incremented
		while(positionOfVectorToBeIncremented == 0){
			int pos = startFromPosition % sorted_dynamo_addr.size();
			String dynamoId = sorted_dynamo_addr.get(pos).split("-")[1];
			positionOfVectorToBeIncremented++;
			if(dynamoId.equals(RestApi.nodeId))
				 break;
			startFromPosition++;
		}
		
		return positionOfVectorToBeIncremented - 1;
		
	}
	
	private String generateVectorClock(String id){
		
		int[] vectorClock = new int[3];
		vectorClock[0] = 0;
		vectorClock[1] = 0;
		vectorClock[2] = 0;
		
		int positionToBeIncremented = vectorPositionToBeIncremented(id);
		
		vectorClock[positionToBeIncremented]++;
		
		String vector = String.valueOf(vectorClock[0]) + "," + String.valueOf(vectorClock[1]) + "," + String.valueOf(vectorClock[2]);
		
		return vector;
	}
	
	public int[] stringVectorToArrayVector(String vector){
		
		String[] elements = vector.split(",");
		int[] vectorClock = new int[3];
		
		for(int i = 0; i < elements.length; i++)
			vectorClock[i] = Integer.parseInt(elements[i]);
			
		return vectorClock;
	}
	
	private String incrementVectorClock(String id, String valueWithVector){
		
		String vector = valueWithVector.split("-")[1];
		int[] vectorClock = stringVectorToArrayVector(vector);
		int positionToBeIncremented = vectorPositionToBeIncremented(id);
		vectorClock[positionToBeIncremented]++;
		
		vector = String.valueOf(vectorClock[0]) + "," + String.valueOf(vectorClock[1]) + "," + String.valueOf(vectorClock[2]);
		
		return vector;
	}
	
	public void saveData(){
		try
	      {
	         FileOutputStream fileOut = new FileOutputStream("values.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(values);
	         out.close();
	         fileOut.close();
	      }catch(IOException i)
	      {
	          i.printStackTrace();
	      }
	}
	
	@SuppressWarnings("unchecked")
	public void loadData(){
		try
	      {
	         FileInputStream fileIn = new FileInputStream("values.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         values = (Map<String, Values>) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i)
	      {
	         i.printStackTrace();
	         return;
	      }catch(ClassNotFoundException c)
	      {
	         System.out.println("Employee class not found");
	         c.printStackTrace();
	         return;
	      }
	}
	
//	public static void main(String[] args){
//		
//      
//		dynamo_addr2.add("192.168.99.100:978-15");
//		dynamo_addr2.add("192.168.99.100:546-25");
//		dynamo_addr2.add("192.168.99.100:421-30");
//		dynamo_addr2.add("192.168.99.100:156-47");
//		dynamo_addr2.add("192.168.99.100:123-56");
//		dynamo_addr2.add("192.168.99.100:345-70");
//		dynamo_addr2.add("192.168.99.100:321-80");
//		
//		String response = putValueAndWriteVersion("testovanie", "90");
//		System.out.println(response);
//		response = putValueAndWriteVersion("ine_testovanie", "90");
//		System.out.println(response);
//		
//	
//	}
	
}
