package rest;

import java.io.IOException;
import java.util.*;

import org.apache.cassandra.dht.*;
import org.apache.cassandra.dht.RandomPartitioner.BigIntegerToken;
import org.apache.cassandra.utils.*;
import org.apache.cassandra.utils.MerkleTree.RowHash;
import org.apache.cassandra.utils.MerkleTree.TreeRange;
import org.apache.cassandra.utils.MerkleTree.TreeRangeIterator;
import org.apache.log4j.Logger;

import static org.apache.cassandra.utils.MerkleTree.RECOMMENDED_DEPTH;

// Used Merkle implementation from Cassandra project http://cassandra.apache.org/
// Implemented thanks to tutorial on http://distributeddatastore.blogspot.sk/2013/07/cassanString nodeWithIntervalString nodeWithIntervaldra-using-merkle-trees-to-detect.html

public class ReplicaSynchronization {
	
	static Logging log = new Logging();
	static Logger logger;
	
	public void doSynchronizationWhenNodeIsAdded(String dynamoIpAddressWithId, String backNeighbourDynamoAddressWithId, String nodeId, List<String> dynamo_interval_addr, DataHandler dataHandler) throws IOException, InterruptedException{
		
		Iterator<String> intervalAddrIter = dynamo_interval_addr.iterator();
		String nodeWithInterval = null;
		while(intervalAddrIter.hasNext()){
			nodeWithInterval = intervalAddrIter.next();
			if(nodeWithInterval.split("-")[1].split("\\.")[1].equals(nodeId))
				break;
		}
		
		String beginningPos;
		String endingPos;
		if(dynamoIpAddressWithId.equals(backNeighbourDynamoAddressWithId)){
			beginningPos = nodeWithInterval.split("-")[1].split("\\.")[0];		
			endingPos = backNeighbourDynamoAddressWithId.split("-")[1];
		} else {
			beginningPos = backNeighbourDynamoAddressWithId.split("-")[1];		
			endingPos = nodeWithInterval.split("-")[1].split("\\.")[1];
		}
		
		String rootHash = buildMerkelTreeAndComputeRootHash(beginningPos, endingPos, dataHandler);
        
        //Send the sync request to the neighbour
        Communicator comm = new Communicator();
        String dynamoAddress = dynamoIpAddressWithId.split("-")[0];
        String senderAddress = nodeWithInterval.split("-")[0];
        comm.synchronize(dynamoAddress, beginningPos, endingPos, rootHash, senderAddress);
		
	}
	
	public void doSynchronizationWhenNodeIsDeleted(String secondBackNeighbourDynamoAddressWithId, String nodeId, List<String> dynamo_interval_addr, DataHandler dataHandler) throws IOException, InterruptedException{
		
		Iterator<String> intervalAddrIter = dynamo_interval_addr.iterator();
		String nodeWithInterval = null;
		while(intervalAddrIter.hasNext()){
			nodeWithInterval = intervalAddrIter.next();
			if(nodeWithInterval.split("-")[1].split("\\.")[1].equals(nodeId))
				break;
		}
		
		String beginningPos = nodeWithInterval.split("-")[1].split("\\.")[0];
		String endingPos = secondBackNeighbourDynamoAddressWithId.split("-")[1];
		
		String rootHash = buildMerkelTreeAndComputeRootHash(beginningPos, endingPos, dataHandler);
        
        //Send the sync request to the neighbour
        Communicator comm = new Communicator();
        String dynamoAddress = secondBackNeighbourDynamoAddressWithId.split("-")[0];
        String senderAddress = nodeWithInterval.split("-")[0];
        comm.synchronize(dynamoAddress, beginningPos, endingPos, rootHash, senderAddress);
		
	}
	
	public void checkIfSynced(String dynamoAddress, String beginningPos, String endingPos, String hash, DataHandler dataHandler) throws IOException, InterruptedException{
		logger = log.start();
		String rootHash = buildMerkelTreeAndComputeRootHash(beginningPos, endingPos, dataHandler);
        
		logger.info(RestApi.nodePort + " - hash je " + hash);
		logger.info(RestApi.nodePort + " - roothash je " + rootHash);
		
        //If both nodes have the same data, no synchronization will occur
        if (rootHash.equals(hash))
        	return;
        
        
        //On the other hand if hashes differ, the node will start sending its data to node, which does not have the actual data
        Communicator comm = new Communicator();
        int beginningPosNo = Integer.parseInt(beginningPos);
		int endingPosNo = Integer.parseInt(endingPos);
		
		while (true){
			beginningPosNo = (beginningPosNo + 1) % RestApi.CIRCLE_SIZE;
			String pos = String.valueOf(beginningPosNo);
			if(dataHandler.hasId(pos)){
				String value = dataHandler.getValue(pos).getValue();
				logger.info(RestApi.nodePort + " - replikujem na " + dynamoAddress + " hodnotu " + value + " s id " + pos);
				comm.replicate(dynamoAddress, dataHandler.getValue(pos).getValue(), pos);
			}
			if(beginningPosNo == endingPosNo)
				break;
		}
        
	}
	
	public static String buildMerkelTreeAndComputeRootHash(String beginningPos, String endingPos, DataHandler dataHandler){
		
		// Do not forget that intervals are like this (beginningPos, endingPos>, first beginningPos is skipped in while loop
		int beginningPosNo = Integer.parseInt(beginningPos);
		int endingPosNo = Integer.parseInt(endingPos);
		Map<BigIntegerToken, byte[]> rows = new TreeMap<BigIntegerToken, byte[]>();
		
		while (true){
			beginningPosNo = (beginningPosNo + 1) % RestApi.CIRCLE_SIZE;
			String pos = String.valueOf(beginningPosNo);
			if(dataHandler.hasId(pos))
				rows.put(new BigIntegerToken(pos), dataHandler.getValue(pos).getValue().getBytes() );
			if(beginningPosNo == endingPosNo)
				break;
		}
		
		// Create Merkle tree of depth 3 for the token range (0, 1024>
        IPartitioner partitioner = new RandomPartitioner();
        Range<Token> fullRange = new Range<Token>(new BigIntegerToken("0"), new BigIntegerToken("1024"));
        MerkleTree tree = new MerkleTree(partitioner, fullRange, RECOMMENDED_DEPTH, 8);
        tree.init();
        addRows(tree, rows);
		
        //Calculate hashes
        MerkleTree.difference(tree, tree);
        
        //Get merkle tree root hash
        String s = tree.toString();
        String rootHash = s.substring(s.indexOf('[')+1, s.indexOf(']'));
		
        return rootHash;
	}
	
	public static void addRows(MerkleTree tree, Map<BigIntegerToken, byte[]> rows) {
        MerkleTree.RowHash EMPTY_ROW = new MerkleTree.RowHash(null, new byte[0], 0);

        // Get the sub range iterator which iterates over sorted sub ranges
        TreeRangeIterator ranges = tree.invalids();
        TreeRange range = ranges.next();

        for (BigIntegerToken token : rows.keySet()) {
            while (!range.contains(token)) {
                // add the empty hash, and move to the next range
                range.addHash(EMPTY_ROW);
                range = ranges.next();
            }
            range.addHash(new RowHash(token, rows.get(token), 0));
        }
        if (range != null) {
            range.addHash(EMPTY_ROW);
        }
        while (ranges.hasNext()) {
            range = ranges.next();
            range.addHash(EMPTY_ROW);
        }
    }

//    public static void main(final String[] args) throws IOException {
//
//    	DataHandler dataHandler = new DataHandler();
//    	dataHandler.putValue("dalsipokus", "200");
//    	checkIfSynced("192.168.99.100:421", "702", "352", "", dataHandler);
////    	Bootstrapper b = new Bootstrapper();
////        List<String> dynamo_addr = new LinkedList<String>();
//        
////        dynamo_addr.add("192.168.99.100:156-47");
////		dynamo_addr.add("192.168.99.100:421-30");
////		dynamo_addr.add("192.168.99.100:546-25");
////		dynamo_addr.add("192.168.99.100:345-70");
////		dynamo_addr.add("192.168.99.100:321-80");
////		dynamo_addr.add("192.168.99.100:978-15");
////		List<String> dynamo_interval_addr = new LinkedList<String>();
////		dynamo_interval_addr = b.makeIntervals(dynamo_addr);
//    	
//    	//doSynchronizationWhenNodeIsAdded("192.168.99.100:546-25", "192.168.99.100:546-25", "30", dynamo_interval_addr, dataHandler);
//		//doSynchronizationWhenNodeIsDeleted("192.168.99.100:546-25", "47", dynamo_interval_addr, dataHandler);
//    	
//    	
////        // Sorted map of hash values of rows
////        String test1 = "pes";
////        String test2 = "macka";
////    	
////    	Map<BigIntegerToken, byte[]> rows1 = new TreeMap<BigIntegerToken, byte[]>();
////        rows1.put(new BigIntegerToken("5"), test1.getBytes() );
////        rows1.put(new BigIntegerToken("135"), new byte[] { (byte)0x0c });
////        rows1.put(new BigIntegerToken("170"), new byte[] { (byte)0x05 });
////        rows1.put(new BigIntegerToken("185"), new byte[] { (byte)0x02 });
////
////        Map<BigIntegerToken, byte[]> rows2 = new TreeMap<BigIntegerToken, byte[]>();
////        rows2.put(new BigIntegerToken("9"), test2.getBytes());
////        rows2.put(new BigIntegerToken("135"), new byte[] { (byte)0x0c });
////        rows2.put(new BigIntegerToken("170"), new byte[] { (byte)0x05 });
////        rows2.put(new BigIntegerToken("185"), new byte[] { (byte)0x02 });
////
////        // Create Merkle tree of depth 3 for the token range (0 - 1024]
////        IPartitioner partitioner = new RandomPartitioner();
////        Range<Token> fullRange = new Range<Token>(new BigIntegerToken("0"), new BigIntegerToken("1024"));
////        MerkleTree tree1 = new MerkleTree(partitioner, fullRange, RECOMMENDED_DEPTH, 8);
////        tree1.init();
////        MerkleTree tree2 = new MerkleTree(partitioner, fullRange, RECOMMENDED_DEPTH, 8);
////        tree2.init();
////
////        // Add row hashes
////        addRows(tree1, rows1);
////        addRows(tree2, rows2);
////
////        // Calculate hashes of non leaf nodes
////        MerkleTree.difference(tree1, tree1);
////        String s1 = tree1.toString();
////        System.out.println("tree1: " + tree1);
////        System.out.println("tree1: " + s1.substring(s1.indexOf('[')+1, s1.indexOf(']')));
////        
////        
////       
////        MerkleTree.difference(tree2, tree2);
////        String s2 = tree2.toString();
////        System.out.println("tree2: " + tree2);
////        System.out.println("tree2: " + s2.substring(s2.indexOf('[')+1, s2.indexOf(']')));
//
//    }	 
}
