package rest;

import static spark.Spark.*;

import java.io.*;
import java.util.*;
import java.util.stream.IntStream;
import java.net.*;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.*;




public class RestApi {
	
	public static final int CIRCLE_SIZE = 1000;
	
	static Values value = new Values("11849","15");
	static DataHandler dataHandler = new DataHandler();
	static List<String[]> temporaryData = new LinkedList<String[]>();
	static Bootstrapper bootstrap = new Bootstrapper();
	static Communicator comm = new Communicator();
	static ConsistentHash hash = new ConsistentHash();
	static ReplicaSynchronization sync = new ReplicaSynchronization();
	static List<String> dynamo_addr = new LinkedList<String>();
	static List<String> dynamo_interval_addr = new LinkedList<String>();
	static Logging log = new Logging();
	static Logger logger;
	static String nodePort;
	static String nodeId;
	static int noOfDynamoNodes;
	static String oldInterval;
	static String newInterval;
	
	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InterruptedException {
		logger = log.start();
		nodePort = args[0];
		nodeId = hash.generateDynamoHash(nodePort);
		//dataHandler.putValue("abcd","10");
		//dataHandler.putValue("aaaa","13");
		
		
		//REST API down here...
		//Same with the REST API as far the communication is concerned, more than 2 nodes are needed for processing requests
		
        // Used for dynamo put operation
		post("/dynamo", (req, res) -> {
        	
        	if(dynamo_addr.size() <= 2){
        		logger.info(nodePort + " - Need to have more than 2 nodes to work properly");
        		return "Need to have more than 2 nodes to work properly";
        	}
        	
        	String val = req.queryParams("value");
        	String id = req.queryParams("id");
        	String write = req.queryParams("write");
        	int writePom = Integer.parseInt(write);
        	
        	if(writePom > 3 || writePom < 1)
        		return "Invalid write number. Dynamo architecture is set to N = 3. \n";
        	
        	int idPom = Integer.parseInt(id);
        	idPom = idPom % CIRCLE_SIZE;
        	String coordinatorAddress = giveMeTheCoordinator(idPom, dynamo_addr);
        	
        	logger.info(nodePort + " - I have received value " + val + " with id " + id + " and coordinator address is " + coordinatorAddress);
        	
        	if(coordinatorAddress != null){
        		
        		//We will check if the coordinator is alive, if not we will save the data and send them later
        		String checkIfAlive = comm.ping(coordinatorAddress);
        		
        		if(checkIfAlive == null){
        			logger.info(nodePort +  " - Node is not alive");
        			String[] data = new String[4];
        			data[0] = coordinatorAddress;
        			data[1] = val;
        			data[2] = id;
        			data[3] = write;
        			temporaryData.add(data);
        			return "Hinted Handoff\n";
        		}
        		
        		logger.info(nodePort + " - Node is alive");
        		String answer = comm.forwardWriteRequest(coordinatorAddress, val, id, write);
        		return answer;
        	}
        	
        	//Counter used for sloppy quorum write
        	int counter = 0;
        	
        	val = dataHandler.putValueAndWriteVersion(val, id);
        	if(dataHandler.hasId(id))
        		++counter;
        	
        	logger.info(nodePort + " - I have saved value " + val + " with id " + id);
        	
        	String replicaAddresses[] = giveMeTheReplicaAddresses(dynamo_addr);
        	logger.info(nodePort + " - My replicas are " + replicaAddresses[0] + " and " + replicaAddresses[1]);
        	
        	for(String replicaAddress : replicaAddresses){
        		String response = comm.replicate(replicaAddress, val, id);
        		if(response != null)
        			++counter;
        	}
        
        	if(counter >= writePom)
        		return "Put operation was successful, " + counter + " nodes answered from " + writePom + " requested.\n";
        	else
        		return "Put operation has failed, " + counter + " nodes answered from " + writePom + " requested.\n";
        	
        }
        );
		
		// Used for replicating
		post("/replicate", (req, res) -> {
          	
        	String val = req.queryParams("value");
        	String id = req.queryParams("id"); 
        	logger.info(nodePort + " - Replikujem hodnotu " + val + " s id " + id);
        	dataHandler.putValue(val, id);
        	if(dataHandler.hasId(id))
        		return "200 OK";
        	else
        		return null;  	              	
        }
        );
		
		// Used for syncing the dynamo nodes
		post("/synchronize", (req, res) -> {
          	
			
			String begin = req.queryParams("begin");
        	String end = req.queryParams("end");
        	String hash = req.queryParams("hash");
        	String sender = req.queryParams("sender");
        	logger.info(nodePort + " - begin je " + begin);
			logger.info(nodePort + " - end je " + end);
			logger.info(nodePort + " - hash je " + hash);
			logger.info(nodePort + " - sender je " + sender);
        	sync.checkIfSynced(sender, begin, end, hash, dataHandler);
        	       	              	
        	return "200 OK";
        }
        ); 
        
        // Used for dynamo get operation
		get("/dynamo", (req, res) -> {
			
        	if(dynamo_addr.size() <= 2){
        		logger.info(nodePort + " - Need to have more than 2 nodes to work properly");
        		return "Need to have more than 2 nodes to work properly";
        	}
        	
        	String id = req.queryParams("id");
        	String read = req.queryParams("read");
        	int readPom = Integer.parseInt(read);
        	
        	if(readPom > 3 || readPom < 1)
        		return "Invalid read number. Dynamo architecture is set to N = 3. \n";
			
        	int idPom = Integer.parseInt(id);
        	idPom = idPom % CIRCLE_SIZE;
        	String coordinatorAddress = giveMeTheCoordinator(idPom, dynamo_addr);
        	
        	logger.info(nodePort + " - I am going to find something that is saved with id " + id);
        	
        	if(coordinatorAddress != null){
        		logger.info(nodePort + " - Going to forward to " + coordinatorAddress);
        		String answer = comm.forwardReadRequest(coordinatorAddress, id, read);
        		
        		if(answer == null)
        			return "Dynamo node has some issues, try later. \n";
        		else
        			return answer;
        	}
        	
        	logger.info(nodePort + " - Inside the right node");
        	
        	String values[] = new String[3];
        	int counter = 0;
        	
        	if(dataHandler.hasId(id)){
        		values[0] = dataHandler.getValue(id).getValue();
        		++counter;
        		logger.info(nodePort + " - First data succesfully taken");
        	}
        	
        	String replicaAddresses[] = giveMeTheReplicaAddresses(dynamo_addr);
        	for(String replicaAddress : replicaAddresses){
        		String response = comm.take(replicaAddress, id);
        		if(response != null){
        			values[counter] = response;
        			++counter;
        			logger.info(nodePort + " - Another data succesfully taken");
        		}
        	}
        	
        	if(counter < readPom)
        		return "Get opearion has failed, " + counter + " nodes answered from " + readPom + " requested. That means we do not have the data, or the nodes are offline\n";
        	
        	int[] sumOfVectorClock = new int[3];
        	sumOfVectorClock[0] = 0;
        	sumOfVectorClock[1] = 0;
        	sumOfVectorClock[2] = 0;
        	
        	//Compute sum of the individual vector clocks
        	for(int i = 0; i < values.length; i++){
        		if(values[i] != null){
        			logger.info(nodePort + " - Value is " + values[i]);
        			int[] vectorClock = dataHandler.stringVectorToArrayVector(values[i].split("-")[1]);
        			sumOfVectorClock[i] = IntStream.of(vectorClock).sum();
        		}
        	}
        	
        	//Save only these values that have the highest vector clock sum number (highest version)
        	int newestVersion = Math.max(Math.max(sumOfVectorClock[0], sumOfVectorClock[1]), sumOfVectorClock[2]);
        	List<String> answers = new LinkedList<String>();
        	for(int i = 0; i < values.length; i++){
        		if(values[i] != null){
        			if(newestVersion == sumOfVectorClock[i])
        				answers.add(values[i]);
        		}
        	}
        	
        	logger.info(nodePort + " - We have " + answers.size() + " answers");
        	
        	//If there are different values with same version, we return them also and let the user decide
        	int numOfDistinctions = (int) answers.stream().distinct().count();
        	String answer = "";
        	
        	switch(numOfDistinctions){
        	//Everyone is same
        	case 1:
        		answer = answers.get(0);
        		logger.info(nodePort + " - Everyone is same");
        		break;
        	
        	//There are two distinctions	
        	case 2:
        		answer = answers.get(0);
        		logger.info(nodePort + " - There are two distinctions");
        		if(!answer.equals(answers.get(1))){
        			answer = answer + ", " + answers.get(1);
        			break;
        		} else {
        			answer = answer + ", " + answers.get(2);
        			break;
        		}
        	
        	//There are three distionctions
        	case 3:
        		answer = answers.get(0) + ", " + answers.get(1) + ", " + answers.get(2);
        		logger.info(nodePort + " - There are three distionctions");
        		break;
        		
        	}
        	
        	return "Get operation was successful, " + counter + " nodes answered from " + readPom + " requested. The searched value is: " + answer + "\n";
        		
		});
		
		// Used for taking the replicated data for read operation
		get("/take/:id", (req, res) -> {
			
        	if(dynamo_addr.size() <= 2){
        		logger.info(nodePort + " - Need to have more than 2 nodes to work properly");
        		return "Need to have more than 2 nodes to work properly";
        	}
        	
        	String id = req.params(":id");
			
        	if(dataHandler.hasId(id))
        		return dataHandler.getValue(id).getValue();
        	else
        		return null;
			
		});
        
        
        // Used for downloading and updating address list along with intervals and also for checking whether to do hinted handoff
        get("/update", (req, res) -> {
        	doBootstrappingProcess(nodeId, false, false);
        	
        	// We have some temporary data, lets check if some nodes are alive
        	if(!temporaryData.isEmpty()){
        		
        		logger.info(nodePort + " - Going to try to do hinted handover");
        		List<Integer> dataToBeRemoved = new LinkedList<Integer>();
        		for(int i = 0; i < temporaryData.size(); i++){
        			String data[] = temporaryData.get(i);
        			String coordinatorAddress = data[0];
        			String val = data[1];
        			String id = data[2];
        			String write = data[3];
        			String checkIfAlive = comm.ping(coordinatorAddress);
        			
        			//If they are alive, we will forward them our temporary data and tag the already sent data, to be removed
        			if(checkIfAlive != null){
            			logger.info(nodePort + " - Node is alive and going to send to " + coordinatorAddress + " value " + val + " with id " + id + " and write " + write);
            			comm.forwardWriteRequest(coordinatorAddress, val, id, write);
        				dataToBeRemoved.add(i);
            		}	
        		}
        		
        		logger.info(nodePort + " - Size of temporary data before removal is " + temporaryData.size());
        		//Here we remove temporary data, that was already sent to its original recipient
        		if(!dataToBeRemoved.isEmpty()){
        			Iterator<Integer> iterDataToBeRemoved = dataToBeRemoved.iterator();
        			while(iterDataToBeRemoved.hasNext()){
        				Integer removeData = iterDataToBeRemoved.next();
        				temporaryData.remove(removeData);
        			}
        			dataToBeRemoved.clear();
        		}
        		logger.info(nodePort + " - Size of temporary data after removal is " + temporaryData.size());
        		logger.info(nodePort + " - Size of dataToBeRemoved list is " + dataToBeRemoved.size());
        		
        	}
        	
        	return "200 OK";
        });
        
        // Used for periodical health checks from consul
        get("/health", (req, res) -> {
        	logger.info(nodePort + " - Health check -  IP: " + req.ip() + ":" + req.port() + " -> 200 OK");
			return "200 OK";
		}
        );
        
        // Used to ping if node is alive
        get("/ping", (req, res) -> {
			return "200 OK";
		}
        );
             
        //First, we wait for our jetty server to initialize, so that we can acquire incoming messages
		awaitInitialization();
		
		//Initiate shutdown hook that listens for SIGTERM signal (docker stop command)
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
	        public void run() {
	            try {
					bootstrap.deregister();
					dataHandler.saveData();
				} catch (IOException | InterruptedException e) {
					logger.error(e);
				}
	        }
	    }, "Shutdown-thread"));
		
		//If data exists, load them and do secondary bootstrap registry, if not do initial boostrap registry 
		File f = new File("values.ser");
		
		if(f.exists() && !f.isDirectory()) {
			dataHandler.loadData();
			doBootstrappingProcess(nodeId, true, false);
		} else {
			doBootstrappingProcess(nodeId, true, true);
		}
		
        
    }
	
	
	//Will register node on startup and download addresses of all registered dynamo nodes and will calculate the circle intervals which nodes are responsible for
	//Intervals will be calculated only when there are more than 2 nodes, becuase this dynamo implementation is based on fixed N = 3
	//Also does the synchronization only when it is added to the circle, or when some node is deleted from the circle
	//If registered initially (newly added to architecture), it will also do register with wait so that it can have all the addresses to initiate replica sync
	//If registered second time (docker start), it will register without wait, because it can receive data from hinted handoff
	static void doBootstrappingProcess(String nodeId, boolean register, boolean initial) throws IOException, InterruptedException{
		if(register && initial)
			bootstrap.registerWithWait(nodeId);
		
		if(register && !initial)
			bootstrap.registerWithoutWait(nodeId);
		
		noOfDynamoNodes = dynamo_addr.size();
		dynamo_addr = bootstrap.getDynamos();
		logger.info(nodePort + " - velkost adries je " + dynamo_addr.size());
		
		if(dynamo_addr.size() > 2){
			dynamo_interval_addr = bootstrap.makeIntervals(dynamo_addr);
			bootstrap.printAddresses(dynamo_interval_addr);
			
			
			newInterval = giveMeMyInterval(dynamo_addr);
			logger.info(nodePort + " - New interval is " + newInterval);
			logger.info(nodePort + " - Old interval was " + oldInterval);
			
			//Do sync - added to the circle
			if(register && initial){
				String neigbourAddresses[] = giveMeTheNeighboursAddresses(dynamo_addr);
				logger.info(nodePort + " - Robim add sync");
				logger.info(nodePort + " - Adresa backneigbourAddress je " + neigbourAddresses[0]);
				logger.info(nodePort + " - Adresa frontneigbourAddress je " + neigbourAddresses[1]);
				sync.doSynchronizationWhenNodeIsAdded(neigbourAddresses[0], neigbourAddresses[0], nodeId, dynamo_interval_addr, dataHandler);
				sync.doSynchronizationWhenNodeIsAdded(neigbourAddresses[1], neigbourAddresses[0], nodeId, dynamo_interval_addr, dataHandler);
				return;
			}
			
			// Do sync - someone was deleted and my interval was changed
			if(noOfDynamoNodes > dynamo_addr.size() && !newInterval.equals(oldInterval)){
				String secondBackneigbourAddress = giveMeTheSecondBackNeighbourAddress(dynamo_addr);
				logger.info(nodePort + " - Robim deleted sync");
				logger.info(nodePort + " - Adresa secondBackneigbourAddress je " + secondBackneigbourAddress);
				sync.doSynchronizationWhenNodeIsDeleted(secondBackneigbourAddress, nodeId, dynamo_interval_addr, dataHandler);
				
			}
			
			oldInterval = giveMeMyInterval(dynamo_addr);
				
		} else {
			logger.info(nodePort + " - Need to have more than 2 nodes to work properly");
		}
	}
	
	
	
	// Checks if this node is the coordinator, if not, it will find the coordinator for the specific position on the circle
    static String giveMeTheCoordinator(int id, List<String> dynamo_addr){
		int min = CIRCLE_SIZE - 1;
		String coordinatorAddress = null;
		
		Iterator<String> dynamoIter = dynamo_addr.iterator();	
		while(dynamoIter.hasNext()){
			String dynamo = dynamoIter.next();
			int pom = Integer.parseInt(dynamo.split("-")[1]);
			int distance = pom - id;
			if(distance < min && distance >= 0)
				min = distance;
		}
		
		// If no min was found, it means that coordinator for highest positions on circle is the first one from the beginning of the circle
		// example: we have circle with ids 0-99 and dynamo nodes on positions 15, 56, 80. Now we want to save value with id 95. Coordinator for this id is 15 and this "if condition" resolves this issue
		if(min == CIRCLE_SIZE - 1){
			Collections.sort(dynamo_addr, new NodeIdComparator());
			String[] dynamoValues = dynamo_addr.get(0).split("-");
			coordinatorAddress = dynamoValues[0];
			String dynamoId = dynamoValues[1];
			if(dynamoId.equals(nodeId))
				return null;
			else
				return coordinatorAddress;
		}
		
		String coordinatorId = Integer.toString(id + min);
		
		// I am the coordinator, I dont have to return any address
		if (coordinatorId.equals(nodeId))
			return null;
		
		// I am not the coordinator, I will find the coordinator address for you
		dynamoIter = dynamo_addr.iterator();
		while(dynamoIter.hasNext()){
			String dynamo = dynamoIter.next();
			String[] dynamoValues = dynamo.split("-");
			coordinatorAddress = dynamoValues[0];
			String dynamoId = dynamoValues[1];
			if(coordinatorId.equals(dynamoId))
				break;
		}
		
		return coordinatorAddress;
	}
    
    // Finds two dynamo node addresses that will be used as replicas for coordinator, basically replicas are first two nodes that are right after coordinator clockwise
	static String[] giveMeTheReplicaAddresses(List<String> dynamo_addr){
    	String[] replicaAddresses = new String[2];
    	int coordinatorPosition = 0;
    	int noOfFoundReplicaAddresses = 0;
    	
    	Collections.sort(dynamo_addr, new NodeIdComparator());
    	
    	for(int i = 0 ; i < dynamo_addr.size(); i++){
    		String dynamoId = dynamo_addr.get(i).split("-")[1];
    		if(dynamoId.equals(nodeId)){
    			coordinatorPosition = i;
    			break;
    		}
    	}
    	
    	int pos = coordinatorPosition;
    	while(noOfFoundReplicaAddresses != 2){
    		pos = (pos + 1) % dynamo_addr.size();
    		replicaAddresses[noOfFoundReplicaAddresses] = dynamo_addr.get(pos).split("-")[0];
    		++noOfFoundReplicaAddresses;
    	}
    	
    	return replicaAddresses;
    }
	
	 // Finds two dynamo node addresses that are neighbours of the specific dynamo node
	static String[] giveMeTheNeighboursAddresses(List<String> dynamo_addr){
    	String[] neigbourAddresses = new String[2];
    	int addedNodePosition = 0;
    	
    	Collections.sort(dynamo_addr, new NodeIdComparator());
    	
    	for(int i = 0 ; i < dynamo_addr.size(); i++){
    		String dynamoId = dynamo_addr.get(i).split("-")[1];
    		if(dynamoId.equals(nodeId)){
    			addedNodePosition = i;
    			break;
    		}
    	}
    	
    	int backNeigbourPosition = (addedNodePosition + dynamo_addr.size() - 1) % dynamo_addr.size();
    	int frontNeigbourPosition = (addedNodePosition + 1) % dynamo_addr.size();
    	
    	//Back neighbour address
    	neigbourAddresses[0] = dynamo_addr.get(backNeigbourPosition);
    	//Front neigbour address
    	neigbourAddresses[1] = dynamo_addr.get(frontNeigbourPosition);	    		    	
    	
    	return neigbourAddresses;
    }
	
	 // Finds dynamo node address that is second back neighbour of the specific dynamo node
	static String giveMeTheSecondBackNeighbourAddress(List<String> dynamo_addr){
    	String secondBackNeighbourAddress;
    	int nodePosition = 0;
    	
    	Collections.sort(dynamo_addr, new NodeIdComparator());
    	
    	for(int i = 0 ; i < dynamo_addr.size(); i++){
    		String dynamoId = dynamo_addr.get(i).split("-")[1];
    		if(dynamoId.equals(nodeId)){
    			nodePosition = i;
    			break;
    		}
    	}
    	
    	int secondBackNeighbourPosition = (nodePosition + dynamo_addr.size() - 2) % dynamo_addr.size();
    	
    	//Second back neighbour address
    	secondBackNeighbourAddress = dynamo_addr.get(secondBackNeighbourPosition);
   	    		    	   	
    	return secondBackNeighbourAddress;
    }
	
	// Returns the interval of responsibility for this specific dynamo node
	static String giveMeMyInterval(List<String> dynamo_addr){
    	String interval;
    	int nodePosition = 0;
    	
    	Collections.sort(dynamo_addr, new NodeIdComparator());
    	
    	for(int i = 0 ; i < dynamo_addr.size(); i++){
    		String dynamoId = dynamo_addr.get(i).split("-")[1];
    		if(dynamoId.equals(nodeId)){
    			nodePosition = i;
    			break;
    		}
    	}
    	
    	//Interval of responsibility for this particular dynamo node
    	interval = dynamo_interval_addr.get(nodePosition).split("-")[1];
   	    		    	   	
    	return interval;
    }

	static void http_get(String ipport, String idcko){
		String url = "http://" + ipport + "/dynamo";
		String charset = java.nio.charset.StandardCharsets.UTF_8.name();  
		String param = idcko;
	
		try {
			String query = String.format("%s", URLEncoder.encode(param, charset));
			
			
			HttpURLConnection connection = (HttpURLConnection) new URL(url + "/" + query).openConnection();
			connection.setRequestMethod("GET");
			
			int status = connection.getResponseCode();
		    System.out.println(status);
		    
		    if (status == 200){
			    InputStream response = connection.getInputStream();
				
				try (Scanner scanner = new Scanner(response)) {
				    String responseBody = scanner.useDelimiter("\\A").next();
				    
				    logger.debug("node: " + ipport + " - id: " + idcko + " - value: " + responseBody);
				}
		    }
			
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
	}
	
	static void http_post(String ipport, String idcko, String value){
		String url = "http://" + ipport + "/dynamo";
		String charset = java.nio.charset.StandardCharsets.UTF_8.name();  
		String param = idcko;
		String param2 = value;
	
		try {
			String query = String.format("value=%s&id=%s", URLEncoder.encode(param2, charset), URLEncoder.encode(param, charset));
			
			HttpURLConnection connection = (HttpURLConnection) new URL(url + "?" + query).openConnection();
			connection.setRequestMethod("POST");
						
			int status = connection.getResponseCode();
		    System.out.println(status);
			
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
	}

}
