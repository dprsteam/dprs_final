package rest;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class ConsistentHash {
		
	// Will generate the hash for node (0-999) which will determine its position on hash circle
	public String generateDynamoHash(String number) throws NoSuchAlgorithmException{

	    MessageDigest m = MessageDigest.getInstance("MD5");
	    m.update(number.getBytes(),0,number.length());
	    String circle_size = String.valueOf(RestApi.CIRCLE_SIZE);
	    BigInteger modulo = new BigInteger(circle_size);
	    BigInteger result = new BigInteger(1,m.digest()).mod(modulo);
	
	    return result.toString(10);
	}	

}