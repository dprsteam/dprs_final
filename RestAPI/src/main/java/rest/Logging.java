package rest;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Logging {
	
	final static Logger logger = Logger.getLogger(RestApi.class.getName());
	static final String path = "/usr/src/app/log4j.properties";
	
	public Logger start(){
		PropertyConfigurator.configure(path);
		return logger;
	}

}
