package rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;


import org.apache.log4j.Logger;

public class Bootstrapper {
	
	final static Logger logger = Logger.getLogger(RestApi.class.getName());
	
	// Will register this node to service discovery and will also save the hash id position of the node on the dynamo circle and will also tell other nodes to download new dynamo address list
	public void registerWithoutWait(String hash) throws IOException, InterruptedException{ 
		String[] command = { "./register.sh", hash };
		Runtime.getRuntime().exec(command);
		logger.info(RestApi.nodePort + " - Registration was successful");
	}
	
	// Will register this node to service discovery and will also save the hash id position of the node on the dynamo circle and will also tell other nodes to download new dynamo address list
	public void registerWithWait(String hash) throws IOException, InterruptedException{ 
		String[] command = { "./register.sh", hash };
		Runtime.getRuntime().exec(command).waitFor();
		logger.info(RestApi.nodePort + " + Registration was successful");
	}
	
	// Will deregister this node from service discovery and will also broadcast to other dynamo nodes to download new dynamo address list
	public void deregister() throws IOException, InterruptedException{
		String[] command = { "./deregister.sh" };
		Runtime.getRuntime().exec(command);   
		logger.info(RestApi.nodePort + " + Deregistration was successful");
	}
	
	// Adds new dynamo addresses along with their hash id to list
	public List<String> getDynamos() throws IOException, InterruptedException{
		List<String> list = new LinkedList<String>();
		String[] command = { "./get_dynamos.sh" };
		
	    Process process = Runtime.getRuntime().exec(command);     
	    process.waitFor();
	    BufferedReader reader = new BufferedReader(new InputStreamReader(        
	    process.getInputStream()));   
	    
	    String s;                                                                
	    while ((s = reader.readLine()) != null)                                
	      list.add(s);
	    
	    Collections.sort(list, new NodeIdComparator());
	    return list;
	}
	
	// Determines the which nodes are responsible for which interval on the dynamo circle and returns new list with addresses and these intervals
	public List<String> makeIntervals(List<String> nodeList){
		List<String> intervalList = new LinkedList<String>();
		int position1 = 0;
		if(nodeList.size() > 0){
			Collections.sort(nodeList, new NodeIdComparator());
			while(position1 < nodeList.size()){
				String endingPos = nodeList.get(position1).split("-")[1];
				int position2 = (position1 - 3 >= 0) ? position1 - 3 : nodeList.size() + (position1 - 3) ;
				String beginningPos = nodeList.get(position2).split("-")[1];
				String complete = nodeList.get(position1).split("-")[0];
				complete = complete + "-" + beginningPos + "." + endingPos;
				intervalList.add(complete);
				++position1;
			}
		}
		
		return intervalList;
	}
	
	// Prints out the addresses for debugging purposes
	public void printAddresses(List<String> dynamo_addr){
		if(dynamo_addr.size() > 0){
			for(int i=0; i<dynamo_addr.size(); i++)
				logger.info(RestApi.nodePort + " - I have address: " + dynamo_addr.get(i));
			}                    
	}
	
//	public static void main(String[] args) throws NoSuchAlgorithmException{
//		ConsistentHash c = new ConsistentHash();
//		String hash = c.generateDynamoHash("1235");
//		System.out.println("Hash is " + hash);
//		
//	}
	
}

	//Comparator used to sort all the dynamo node's hash id in ascending order
	class NodeIdComparator implements Comparator<String> {
	
		@Override
		public int compare(String o1, String o2) {
			String value1[] = o1.split("-");
			String value2[] = o2.split("-");
			
			return Integer.compare(Integer.valueOf(value1[1]), Integer.valueOf(value2[1]));
		}
		
	}