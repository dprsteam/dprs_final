#!/bin/bash 
source env.sh 

rm -rf docker-compose.yml 
envsubst < "template.yml" > "docker-compose.yml"
envsubst < "./filebeat/template.yml" > "./filebeat/filebeat.yml"
docker-compose up -d

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/faultTolerance.sh &> faultTolerance.log &
$DIR/check.sh &> check_health.log &

