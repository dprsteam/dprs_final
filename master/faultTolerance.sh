#!/bin/bash 
source env.sh

dynamo=dynamo
max_fault=5

init () {

while [ 1 ]
do
	json_catalog_services=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/catalog/services)

	if (echo "$json_catalog_services" | grep -q $dynamo) then

		json_dynamo=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/catalog/service/$dynamo)
		json_health_dynamo=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/health/service/$dynamo)

	        read -r -a dynamo_address <<< $(echo $json_dynamo | grep -oP '(?<=ServiceAddress).*?(?=}|$)' | awk -F "\"" '{print $3$6}') 
		read -r -a dynamo_status <<< $(echo $json_health_dynamo | grep -oP '(?<=Checks).*?(?=}|$)' | grep -oP '(?<=Status).*?(?=}|$)' | awk -F "\"" '{print $3}')
		
		for (( m=0; m<${#dynamo_address[@]}; m++ ));
		do
			dynamo_fault[$m]=0
		done  
		return
	else
		echo $(date +'%d/%m/%Y %T') - no $dynamo service
	fi
  sleep 5
done
}

unregister() {
read -r -d '' MSG << EOM
	{
	    "Node": "slave_node",
  	    "ServiceID": "$dynamo-$1"
	}
EOM
curl -s -X PUT -d "$MSG" http://${VM_MASTER_IP}:8500/v1/catalog/deregister
}

broadcast_update() {
# curl -XGET http://ip_slave:port/update
	echo sending broadcast update
	for (( i=0; i<${#dynamo_address[@]}; i++ ));
	do
	        if [ ${dynamo_status[$i]} = "passing" ]; then
			curl -s -XGET http://${dynamo_address[$i]}/update	
		fi
	done
}

diff(){
    a1="$1"
    a2="$2"
    awk -va1="$a1" -va2="$a2" '
     BEGIN{
       m= split(a1, A1," ")
       n= split(a2, t," ")
       for(i=1;i<=n;i++) { A2[t[i]] }
       for (i=1;i<=m;i++){
            if( ! (A1[i] in A2)  ){
                printf A1[i]" "
            }
        }
    }'
}

unset_node(){
	for (( i=0; i<${#dynamo_address[@]}; i++ ));
	do
		if [ ${dynamo_address[$i]} = $1 ]; then
			unset dynamo_address[$i]
			unset dynamo_status[$i]
			unset dynamo_fault[$i]
			read -r -a dynamo_address <<< $(echo ${dynamo_address[*]})
			read -r -a dynamo_status <<< $(echo ${dynamo_status[*]})
			read -r -a dynamo_fault <<< $(echo ${dynamo_fault[*]})
		fi
	done
}

kill_dynamo() {
	port=$(echo $1 | cut -d ":" -f 2)
	#ServiceID: dynamo-$port

	eval $(docker-machine env slave1)
	container_ID=$(docker ps | grep :$port | tr -s " " | cut -d " " -f 1)
	docker kill $container_ID
	eval $(docker-machine env master)

	unregister $port
	unset_node $1
	broadcast_update
}

init 

while [ 1 ]
do
  json_catalog_services=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/catalog/services)

  if (echo "$json_catalog_services" | grep -q $dynamo) then

	json_dynamo=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/catalog/service/$dynamo)
	json_health_dynamo=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/health/service/$dynamo)

	read -r -a actual_address <<< $(echo $json_dynamo | grep -oP '(?<=ServiceAddress).*?(?=}|$)' | awk -F "\"" '{print $3$6}') 
	read -r -a actual_status <<< $(echo $json_health_dynamo | grep -oP '(?<=Checks).*?(?=}|$)' | grep -oP '(?<=Status).*?(?=}|$)' | awk -F "\"" '{print $3}')

	if [ ${#actual_address[@]} -gt ${#dynamo_address[@]} ]; then
		news=$(expr ${#actual_address[@]} - ${#dynamo_address[@]})	
		for (( i=$news; i>0; i-- ))	
		do	
			dynamo_address[${#dynamo_address[@]}]=${actual_address[${#actual_address[@]} - $i]}
			dynamo_status[${#dynamo_status[@]}]=${actual_status[${#actual_address[@]}] - $i}
			dynamo_fault[${#dynamo_fault[@]}]=0
		done
        fi

	if [ ${#dynamo_address[@]} -gt ${#actual_address[@]} ]; then
		a1=${dynamo_address[@]};a2=${actual_address[@]}
		diff_address=( $(diff "$a1" "$a2") )  
		for (( i=0; i<${#dynamo_address[@]}; i++ ));
		do
			unset_node ${diff_address[@]}
		done
        fi
	
	for (( i=0; i<${#dynamo_address[@]}; i++ ));
	do
		for (( j=0; j<${#actual_address[@]}; j++ ));
		do
		        if [ ${dynamo_address[$i]} = ${actual_address[$j]} ]; then
			        if [ ${actual_status[$j]} != "passing" ]; then
					dynamo_status[$i]=${actual_status[$j]}
					((dynamo_fault[$i]++))
					if ((${dynamo_fault[$i]} == 5)); then
						echo ${dynamo_address[$i]} - fault: ${dynamo_fault[$i]} - killing
						kill_dynamo ${dynamo_address[$i]}
						break
					fi					
				elif [ ${actual_status[$j]} = "passing" ]; then
					dynamo_status[$i]=${actual_status[$j]}
					dynamo_fault[$i]=0
				fi
       			fi
		done
	echo $(date +'%d/%m/%Y %T')  -  ${dynamo_address[$i]} - ${dynamo_status[$i]} - fault: ${dynamo_fault[$i]}
	done
	echo " "
  elif [ ${#dynamo_address[@]} -gt 0 ]; then
	echo $(date +'%d/%m/%Y %T') - missing $dynamo service
  fi

  sleep 5
done


