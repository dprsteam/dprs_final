#!/bin/bash 

docker-compose down
ps -a | grep check | awk '{print $1}'  | xargs kill
ps -a | grep faultTolerance | awk '{print $1}'  | xargs kill

