#!/bin/bash 
export DOCKER_IP=$(ifconfig docker0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)
export VM_MASTER_IP=$(docker-machine ip master)
export VM_SLAVE_IP=$(docker-machine ip slave1)
