#!/bin/bash
source env.sh

dynamo=dynamo

init () {

while [ 1 ]
do
	json_catalog_services=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/catalog/services)

	if (echo "$json_catalog_services" | grep -q $dynamo) then
		return
	else
		echo $(date +'%d/%m/%Y %T') - no $dynamo service
	fi
  sleep 5
done
}

update(){ # ip:port status
	ip=$(echo $1 | cut -d ":" -f 1)	
	port=$(echo $1 | cut -d ":" -f 2)

read -r -d '' MSG << EOM
	{	
		"Node": "slave_node",
   		"Address": "$DOCKER_IP",
  	    "Service": {
			"ID": "$dynamo-$port",			
			"Service": "$dynamo", 
			"Tags": [
				"production", "$2"
			],			
			"Address": "$ip",	
			"Port": $port
		},
		"Check": {
			"CheckID": "service:$dynamo-$port",
 		        "ServiceID": "$dynamo-$port",
			"Name": "Dynamo health check",	 	
			"Status": "$3"
		}
	}
EOM
curl -s -X PUT -d "$MSG" http://$VM_MASTER_IP:8500/v1/catalog/register
}

init 

while [ 1 ]
do
	json_catalog_services=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/catalog/services)

	if (echo "$json_catalog_services" | grep -q $dynamo) then

		json_dynamo=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/catalog/service/$dynamo)
		read -r -a dynamo_address <<< $(echo $json_dynamo | grep -oP '(?<=ServiceAddress).*?(?=}|$)' | awk -F "\"" '{print $3$6}') 
		read -r -a dynamo_tags <<< $(echo $json_dynamo | grep -oP '(?<=ServiceTags).*?(?=}|$)' | awk -F "\"" '{print $5}')
 		json_health_dynamo=$(curl -s -XGET http://${VM_MASTER_IP}:8500/v1/health/service/$dynamo)
		read -r -a consul_dynamo_status <<< $(echo $json_health_dynamo | grep -oP '(?<=Checks).*?(?=}|$)' | grep -oP '(?<=Status).*?(?=}|$)' | awk -F "\"" '{print $3}')

		for (( i=0; i<${#dynamo_address[@]}; i++ ));
		do
			status=$(curl -s -XGET http://${dynamo_address[$i]}/health)
		        if [ "${status[@]}" = "200 OK" ]; then
				if [ "${consul_dynamo_status[$i]}" = "critical" ]; then
					update ${dynamo_address[$i]} ${dynamo_tags[$i]} "passing"
				fi
				echo $(date +'%d/%m/%Y %T') - ${dynamo_address[$i]} - health: ${status[@]}
			else
				if [ "${consul_dynamo_status[$i]}" != "critical" ]; then
					update ${dynamo_address[$i]} ${dynamo_tags[$i]} "critical"
				fi
				echo $(date +'%d/%m/%Y %T') - ${dynamo_address[$i]} - health: "critical"
			fi
		done  
		echo " "
	fi
  sleep 5
done
