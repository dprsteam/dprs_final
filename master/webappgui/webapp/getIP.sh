#!/bin/bash
curl -XGET ${VM_MASTER_IP}:8500/v1/catalog/service/dynamo 2>&1 | grep -oP '(?<=ServiceAddress).*?(?=}|$)' | awk -F "\"" '{print $3$6}'
