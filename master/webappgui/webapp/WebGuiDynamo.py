import os
import web
import httplib
import urllib2
import subprocess
from web import form

render = web.template.render('templates/')
urls = ('/', 'index')
app = web.application(urls, globals())
u=3

myform = form.Form( 
	form.Textbox("ID"), 
	form.Textbox("Value"),
	form.Textbox("r"),
	form.Textbox("w"),
	form.Dropdown('Request', ['get', 'post']) 
	)
class index: 
	def GET(self): 
		form = myform()
		return render.formtest(form)

	def POST(self): 
		form = myform() 
		if not form.validates(): 
			return render.formtest(form)
		else:
			if form.d.Request == "get":
				answer = subprocess.check_output(['./getIP.sh'])
				pole = answer.split()
				conn = httplib.HTTPConnection(os.environ['VM_MASTER_IP'])
				conn.request("GET", "http://"+os.environ['VM_MASTER_IP']+"/dynamo?id=" + form.d.ID + "&read=" + form.d.r)
				r1 = conn.getresponse()
				return r1.read()
				conn.close()
			elif form.d.Request == "post":
				answer = subprocess.check_output(['./getIP.sh'])
				pole = answer.split()
				conn = httplib.HTTPConnection(os.environ['VM_MASTER_IP'])
				conn.request("POST", "http://"+os.environ['VM_MASTER_IP']+"/dynamo?value=" + form.d.Value + "&id=" + form.d.ID + "&write=" + form.d.w)
				r1 = conn.getresponse()
				return r1.read()
				conn.close() 
if __name__=="__main__":
	web.internalerror = web.debugerror
	app.run()
